namespace CarsRegistrationApp.Tests
{
	[TestClass]
	public class CarRegistrationTests
	{
		[TestMethod]
		public void RegisterCarShouldRegisterCarSuccessfully()
		{
			// Arrange

			// Act

			// Assert
			throw new NotImplementedException();
		}

		[TestMethod]
		public void RegisterCarShouldFailToRegisterWhenCarAlreadyExists()
		{
			// Arrange

			// Act

			// Assert
			throw new NotImplementedException();
		}

		[TestMethod]
		public void GetOwnerShouldReturnTrueAndOwnerNameIfCarExists()
		{
			// Arrange

			// Act

			// Assert
			throw new NotImplementedException();
		}

		[TestMethod]
		public void GetOwnerShouldReturnFalseIfCarDoesNotExist()
		{
			// Arrange

			// Act

			// Assert
			throw new NotImplementedException();
		}

		[TestMethod]
		public void GetAllCars_ShouldReturnAllRegisteredCars()
		{
			// Arrange

			// Act

			// Assert
			throw new NotImplementedException();
		}

		[TestMethod]
		public void GetAllCarsShouldReturnNoCarsRegisteredMessageWhenEmpty()
		{
			// Arrange

			// Act

			// Assert
			throw new NotImplementedException();
		}

		[TestMethod]
		public void SaveToFileShouldSaveAllCarsToFile()
		{
			// Arrange

			// Act

			// Assert

			// Cleanup (delete file)
			throw new NotImplementedException();
		}

		[TestMethod]
		public void SaveToFileShouldCreateFileIfNotExists()
		{
			// Arrange

			// Act

			// Assert

			// Cleanup (delete file)
			throw new NotImplementedException();
		}

		[TestMethod]
		public void LoadFromFileShouldLoadCarsFromFile()
		{
			// Arrange

			// Act

			// Assert

			// Cleanup (delete file)
			throw new NotImplementedException();
		}

		[TestMethod]
		public void LoadFromFileShouldClearExistingDataBeforeLoading()
		{
			// Arrange

			// Act

			// Assert

			// Cleanup (delete file)
			throw new NotImplementedException();
		}
	}
}