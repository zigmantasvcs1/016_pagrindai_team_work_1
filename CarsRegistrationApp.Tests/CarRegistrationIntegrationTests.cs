﻿namespace CarsRegistrationApp.Tests
{
	[TestClass]
	public class ProgramTests
	{
		private StringWriter stringWriter;
		private StringReader stringReader;

		[TestInitialize]
		public void TestInitialize()
		{
			stringWriter = new StringWriter();
			Console.SetOut(stringWriter);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			Console.SetIn(new StreamReader(Console.OpenStandardInput()));
			Console.SetOut(new StreamWriter(Console.OpenStandardOutput()));
			stringWriter = null;
			stringReader = null;
		}

		[TestMethod]
		public void RunShouldPrintMenuAndExitWhenChoiceIs6()
		{
			// Arrange
			var input = "6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsTrue(output.Contains("Car Registration Menu:"));
			Assert.IsTrue(output.Contains("1. Register Car"));
			Assert.IsTrue(output.Contains("2. Get Owner"));
			Assert.IsTrue(output.Contains("3. Print All Cars"));
			Assert.IsTrue(output.Contains("4. Save to File"));
			Assert.IsTrue(output.Contains("5. Load from File"));
			Assert.IsTrue(output.Contains("6. Exit"));
			Assert.IsTrue(output.Contains("Enter your choice: "));
		}

		[TestMethod]
		public void RunShouldPrintInvalidChoiceWhenChoiceIsNot1To6()
		{
			// Arrange
			var input = "7\n6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsTrue(output.Contains("Invalid choice. Please try again."));
		}

		[TestMethod]
		public void RunShouldRegisterCarWhenChoiceIs1AndHasWriterPermission()
		{
			// Arrange
			var licensePlate = "ABC123";
			var owner = "John Doe";

			var input = $"1\n{licensePlate}\n{owner}\n6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			Auth.CurrentRights = Auth.Writer;

			// Act
			CarRegistrationUI.Run();

			// Assert
			Assert.IsTrue(CarRegistration.Cars.ContainsKey(licensePlate));
			Assert.AreEqual(owner, CarRegistration.Cars[licensePlate]);
		}

		[TestMethod]
		public void RunShouldRegisterCarWithWritePermission()
		{
			// Arrange
			Auth.CurrentRights = Auth.Writer;

			var input = "1\nABC123\nJohn Doe\n6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			Assert.IsTrue(CarRegistration.Cars.ContainsKey("ABC123"));
			Assert.AreEqual("John Doe", CarRegistration.Cars["ABC123"]);
		}

		[TestMethod]
		public void RunShouldNotRegisterCarWithoutWritePermission()
		{
			// Arrange
			Auth.CurrentRights = Auth.Reader;

			var input = "1\n6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsTrue(output.Contains("Access denied. You do not have write permissions."));
		}

		[TestMethod]
		public void RunShouldGetOwnerWithReadPermission()
		{
			// Arrange
			Auth.CurrentRights = Auth.Reader;
			CarRegistration.RegisterCar("ABC123", "John Doe");
			var input = "2\nABC123\n6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsTrue(output.Contains("John Doe"));
		}

		[TestMethod]
		public void RunShouldNotGetOwnerWithoutReadPermission()
		{
			// Arrange
			Auth.CurrentRights = Auth.NoAccess; // Not enough permission
			CarRegistration.RegisterCar("ABC123", "John Doe");

			var input = "2\nABC123\n6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsFalse(output.Contains("John Doe"));
		}

		[TestMethod]
		public void RunShouldSaveToFileWithAdminPermission()
		{
			// Arrange
			Auth.CurrentRights = Auth.Admin;
			CarRegistration.RegisterCar("ABC123", "John Doe");

			var filePath = Path.GetTempFileName();

			var input = $"4\n{filePath}\n6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			Assert.IsTrue(File.Exists(filePath));
			File.Delete(filePath); // Cleanup
		}

		[TestMethod]
		public void RunShouldNotSaveToFileWithoutAdminPermission()
		{
			// Arrange
			Auth.CurrentRights = Auth.Reader; // Not enough permission
			CarRegistration.RegisterCar("ABC123", "John Doe");

			var input = $"4\n6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsTrue(output.Contains("Access denied. You need admin permissions."));
		}
	}
}
