﻿namespace CarsRegistrationApp.Tests
{
	[TestClass]
	public class AuthTests
	{
		private StringWriter? stringWriter;
		private StringReader? stringReader;

		[TestInitialize]
		public void TestInitialize()
		{
			stringWriter = new StringWriter();
			Console.SetOut(stringWriter);
			Console.SetIn(new StreamReader(Console.OpenStandardInput()));
		}

		[TestCleanup]
		public void TestCleanup()
		{
			Console.SetIn(new StreamReader(Console.OpenStandardInput()));
			Console.SetOut(new StreamWriter(Console.OpenStandardOutput()));
			stringWriter = null;
			stringReader = null;
		}

		[TestMethod]
		public void SetPermissionsShouldSetReaderPermission()
		{
			// Arrange
			stringReader = new StringReader("1");
			Console.SetIn(stringReader);

			// Act
			Auth.SetPermissions();

			// Assert
			Assert.AreEqual(Auth.Reader, Auth.CurrentRights);
		}

		[TestMethod]
		public void SetPermissionsShouldSetWriterPermission()
		{
			// Arrange
			stringReader = new StringReader("2");
			Console.SetIn(stringReader);

			// Act
			Auth.SetPermissions();

			// Assert
			Assert.AreEqual(Auth.Writer, Auth.CurrentRights);
		}

		[TestMethod]
		public void SetPermissionsShouldSetAdminPermission()
		{
			// Arrange
			stringReader = new StringReader("3");
			Console.SetIn(stringReader);

			// Act
			Auth.SetPermissions();

			// Assert
			Assert.AreEqual(Auth.Admin, Auth.CurrentRights);
		}

		[TestMethod]
		public void SetPermissionsShouldSetNoAccessOnInvalidInput()
		{
			// Arrange
			stringReader = new StringReader("invalid\n1"); // Invalid input followed by a valid one
			Console.SetIn(stringReader);

			// Act
			Auth.SetPermissions();

			// Assert
			var output = stringWriter?.ToString();
			Assert.IsTrue(output?.Contains("No permissions"));
			Assert.AreEqual(Auth.NoAccess, Auth.CurrentRights);
		}
	}
}
