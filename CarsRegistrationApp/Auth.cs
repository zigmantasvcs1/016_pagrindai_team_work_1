﻿namespace CarsRegistrationApp
{
	public class Auth
	{
		public static int CurrentRights = 0b0000;
		public static int NoAccess = 0b0000;
		public static int Reader = 0b0001;
		public static int Writer = 0b0011;
		public static int Admin = 0b0111;

		public static void SetPermissions()
		{
			bool noChoice = true;

			do
			{
				Console.WriteLine("\nChose your role:");
				Console.WriteLine("1. Reader");
				Console.WriteLine("2. Writer");
				Console.WriteLine("3. Admin");
				Console.Write("Enter your choice: ");

				string choice = Console.ReadLine();

				switch (choice)
				{
					case "1":
						Console.WriteLine("Reader permission set");
						CurrentRights = Reader;
						break;
					case "2":
						Console.WriteLine("Writer permission set");
						CurrentRights = Writer;
						break;
					case "3":
						Console.WriteLine("Admin permission set");
						CurrentRights = Admin;
						break;
					default:
						Console.WriteLine("No permissions");
						CurrentRights = NoAccess;
						break;
				}
			} while (!noChoice);
		}
	}
}
