﻿namespace CarsRegistrationApp
{
	public static class CarRegistrationUI
	{
		/// <summary>
		/// Starts the main loop for the Car Registration application.
		/// This loop displays a menu of options for the user to interact with the car registry.
		/// Options include: Registering a car, retrieving owner information,
		/// printing all cars, saving data to a file, loading data from a file, and exiting the program.
		/// </summary>
		public static void Run()
		{
			bool running = true;
			while (running)
			{
				Console.WriteLine("\nCar Registration Menu:");
				Console.WriteLine("1. Register Car");
				Console.WriteLine("2. Get Owner");
				Console.WriteLine("3. Print All Cars");
				Console.WriteLine("4. Save to File");
				Console.WriteLine("5. Load from File");
				Console.WriteLine("6. Exit");
				Console.Write("Enter your choice: ");
				string choice = Console.ReadLine();

				switch (choice)
				{
					case "1":
						if((Auth.CurrentRights & Auth.Writer) == Auth.Writer)
						RegisterCar();
						break;
					case "2":
						GetOwner();
						break;
					case "3":
						PrintAllCars();
						break;
					case "4":
						SaveToFile();
						break;
					case "5":
						LoadFromFile();
						break;
					case "6":
						running = false;
						break;
					default:
						Console.WriteLine("Invalid choice. Please try again.");
						break;
				}
			}
		}

		/// <summary>
		/// Interacts with the user to register a new car in the car registry.
		/// It prompts the user to input the license plate and the owner's name.
		/// If the registration is successful, a success message is displayed;
		/// if the car is already registered, an error message is shown.
		/// </summary>
		private static void RegisterCar()
		{
			Console.WriteLine("aaa");
			var licensePlate = Console.ReadLine();
			var owner = Console.ReadLine();

			CarRegistration.RegisterCar(licensePlate, owner);
		}

		/// <summary>
		/// Interacts with the user to retrieve the owner of a car from the car registry.
		/// It prompts the user to input the license plate.
		/// If the retrieval is successful, the owner's name is displayed;
		/// if the owner is not found, an error message is shown.
		/// </summary>
		private static void GetOwner()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Prints all registered cars and their corresponding owners to the console.
		/// This data is retrieved from the car registry.
		/// </summary>
		private static void PrintAllCars()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Interacts with the user to save the car registry data to a file.
		/// It prompts the user to input the file path.
		/// If the save operation is successful, a success message is displayed;
		/// if an error occurs during the process, the error message is shown.
		/// </summary>
		private static void SaveToFile()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Interacts with the user to load car registry data from a file.
		/// It prompts the user to input the file path.
		/// If the load operation is successful, a success message is displayed;
		/// if an error occurs during the process, the error message is shown.
		/// </summary>
		private static void LoadFromFile()
		{
			throw new NotImplementedException();
		}
	}
}
