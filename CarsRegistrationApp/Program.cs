﻿namespace CarsRegistrationApp
{
	internal class Program
	{
		static void Main(string[] args)
		{
			Auth.SetPermissions();
			CarRegistrationUI.Run();
		}
	}
}